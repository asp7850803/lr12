﻿using Microsoft.AspNetCore.SignalR;
using System.Net;
using System.Timers;
using WebApplication21.Models.Database;
using WebApplication21.Models.Dto;
using WebApplication21.Services.CurrencyService;

namespace WebApplication21.Hubs
{
    public interface ICurrencyHub
    {
        public Task RecieveCurrencyRate(ResponseDto<APIResponseDto> dto);
        public Task HandleException(ResponseDto<Exception> dto);
        public Task Test(string message);
        public Task ConnectionRemoved(string message);
    }

    public class CurrencyHub : Hub<ICurrencyHub>
    {
        private readonly ICurrencyService _currencyService;

        private readonly List<UserData> _users = new List<UserData>();
        private static System.Timers.Timer _timer = new System.Timers.Timer(2000);

        public CurrencyHub(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
            _timer.Elapsed += SendDataByTimer;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }


        public async Task SendCurrencyData(string baseCurrency, string exchangeCurrencies)
        {
            try
            {
                var user = new UserData() {Id=$"{baseCurrency}{Context.ConnectionId}{exchangeCurrencies}", BaseCurrency = baseCurrency, ConnectionId = Context.ConnectionId, CurrenciesExchange = exchangeCurrencies };
                _users.Add(user);
                Console.WriteLine(user.ConnectionId);
                var response = await this._currencyService.Get(baseCurrency, exchangeCurrencies);
                var responseDto = new ResponseDto<APIResponseDto>()
                {
                    Data = new List<APIResponseDto> { response },
                    StatusCode = HttpStatusCode.OK
                };
                await Clients.Caller.RecieveCurrencyRate(responseDto);
            }
            catch (Exception ex)
            {
                await this.HandleSocketException(ex);
            }
        }

        public async void SendDataByTimer(Object source, ElapsedEventArgs e)
        {
            if (_users.Count > 0)
            {
                foreach (var user in _users)
                {
                    try
                    {
                        var response = await this._currencyService.Get(user.BaseCurrency, user.CurrenciesExchange);
                        var responseDto = new ResponseDto<APIResponseDto>()
                        {
                            Data = new List<APIResponseDto> { response },
                            StatusCode = HttpStatusCode.OK
                        };
                        await Clients.Client(user.ConnectionId).RecieveCurrencyRate(responseDto);
                    }
                    catch (Exception ex)
                    {
                        await this.HandleSocketException(ex);
                    }
                }
            }

        }

        public async Task RemoveConnection(string baseCurrency, string exchangeCurrencies)
        {
            try
            {
                var removedUser = _users.FirstOrDefault((element) => element.Id ==$"{baseCurrency}{Context.ConnectionId}{exchangeCurrencies}");
                _users.Remove(removedUser);
                await Clients.Caller.ConnectionRemoved("Connection was removed");
                if (_users.Count == 0)
                {
                    _timer.Stop();
                }
            }
            catch (Exception ex)
            {
                await this.HandleSocketException(ex);
            }
        }

        public async Task HandleSocketException(Exception ex)
        {
            var responseDto = new ResponseDto<Exception>()
            {
                Data = new List<Exception> { ex },
                StatusCode = HttpStatusCode.InternalServerError
            };
            await Clients.Client(Context.ConnectionId).HandleException(responseDto);
        }
    }
}
