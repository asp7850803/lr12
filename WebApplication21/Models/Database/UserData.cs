﻿namespace WebApplication21.Models.Database
{
    public class UserData
    {
        public string Id { get; set; }
        public string ConnectionId { get; set; }
        public string BaseCurrency { get; set; }
        public string CurrenciesExchange { get; set; }
    }
}
