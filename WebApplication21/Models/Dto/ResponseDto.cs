﻿using System.Net;

namespace WebApplication21.Models.Dto
{
    public class ResponseDto<T>
    {
        public List<T> Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
