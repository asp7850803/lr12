﻿using System.Text.Json;
using WebApplication21.Models.Dto;

namespace WebApplication21.Services.CurrencyService
{
    public class CurrencyService : ICurrencyService
    {
        private readonly HttpClient _HttpClient;
        private readonly string _apiKey;

        public CurrencyService(IConfiguration configuration) {
            _HttpClient = new HttpClient { BaseAddress = new Uri("https://api.currencybeacon.com/v1/latest") };
            _apiKey = configuration.GetValue<string>("APIKey");
        }

        public async Task<APIResponseDto> Get(string baseCurrency, string exchangeCurrencies)
        {
            var response = await _HttpClient.GetAsync($"?api_key={_apiKey}&base={baseCurrency}&symbols={exchangeCurrencies}");
            response.EnsureSuccessStatusCode();

            var responseData = await response.Content.ReadAsStringAsync();
            var data = JsonSerializer.Deserialize<APIResponseDto>(responseData);
            return data;
        }
    }
}
