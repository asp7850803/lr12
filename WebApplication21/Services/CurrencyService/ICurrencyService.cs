﻿using WebApplication21.Models.Dto;

namespace WebApplication21.Services.CurrencyService
{
    public interface ICurrencyService
    {
        public Task<APIResponseDto> Get(string baseCurrency, string exchangeCurrencies);
    }
}
